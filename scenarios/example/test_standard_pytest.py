"""This is a 'normal' pytest routine."""

from typing import List, Tuple

import pytest


def get_mtca_crates() -> List[Tuple[str, str]]:
    mtca_crate_lst = []
    for i in range(3):
        mtca_crate_lst.append((str(i), "fake"))

    return mtca_crate_lst


@pytest.mark.parametrize("pv", get_mtca_crates())
def test_mtca_crate(pv: Tuple[str, str]):
    key, val = pv
    assert key == val
