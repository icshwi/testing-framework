"""Local (to scenario) pytest configuration file."""

from pathlib import Path

from etf.yaml import collect_metadata


def pytest_configure(config):
    config_path = Path(__file__).absolute().parent / "metadata.yml"
    for key, val in collect_metadata(config_path).items():
        config._metadata[key] = val
