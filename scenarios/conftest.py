"""Global pytest configuration file."""

from pathlib import Path

from etf.yaml import YamlConfigItem, YamlFile, collect_metadata


def pytest_configure(config):
    config_path = Path(__file__).absolute().parent / "metadata.yml"
    for key, val in collect_metadata(config_path).items():
        config._metadata[key] = val


def pytest_collect_file(parent, path):
    if path.ext in (".yaml", ".yml") and path.basename.startswith("test"):
        return YamlFile.from_parent(parent, fspath=path)


def pytest_collection_modifyitems(session, config, items):
    config_items = [item for item in items if isinstance(item, YamlConfigItem)]
    for category in config_items:
        if hasattr(config, "_metadata"):
            config._metadata.update(category.spec.items())
        items.remove(category)
