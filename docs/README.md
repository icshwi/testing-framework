# Report examples

## CLI

```sh
$ etf scenarios/example/test_simple.yml
================================================= test session starts ==================================================
platform linux -- Python 3.8.5, pytest-6.1.2, py-1.10.0, pluggy-0.13.1 -- /mnt/c/dev/testing-framework/venv/bin/python3
cachedir: .pytest_cache
metadata: {'Python': '3.8.5', 'Platform': 'Linux-4.19.104-microsoft-standard-x86_64-with-glibc2.29', 'Packages': {'pytest': '6.1.2', 'py': '1.10.0', 'pluggy': '0.13.1'}, 'Plugins': {'html': '3.1.1', 'metadata': '1.11.0'}, 'The': 'quick', 'Brown': {'fox': 'jumps', 'over': 'the', 'lazy': ['dog']}, 'Organization': 'European Spallation Source ERIC', 'Country': 'Sweden'}
rootdir: /mnt/c/dev/testing-framework
plugins: html-3.1.1, metadata-1.11.0
collected 4 items

scenarios/example/test_simple.yml::foo PASSED                                                                    [ 50%]
scenarios/example/test_simple.yml::bar FAILED                                                                    [100%]

======================================================= FAILURES =======================================================
_________________________________________________________ bar __________________________________________________________

error = assert

    assert: false
    name: bar

/mnt/c/dev/testing-framework/scenarios/example/test_simple.yml:2: YamlAssertionError
=============================================== short test summary info ================================================
FAILED scenarios/example/test_simple.yml::bar
============================================= 1 failed, 1 passed in 0.60s ==============================================
```

## HTML

[Example](report.html).

## PDF

[Example](report.pdf).
