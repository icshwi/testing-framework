import setuptools

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

setuptools.setup(
    name="etf",  # working name
    version="0.1.0",
    author="Anders Lindh Olsson",
    author_email="anders.lindholsson@ess.eu",
    description="A testing framework based on pytest",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.esss.lu.se/anderslindh1/testing-framework",
    scripts=["bin/etf"],
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=[
        "pytest",
        "pytest-metadata",
        "pytest-html",
        "PyYAML",
    ],
)
