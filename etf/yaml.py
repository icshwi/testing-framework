"""Module to process YAML-files and create tests out of these."""

import textwrap
from pathlib import Path

import pytest
import yaml


def collect_metadata(file: Path) -> dict:
    if file.exists():
        with open(file, "r") as f:
            return yaml.load(f, Loader=yaml.FullLoader)


class YamlException(Exception):
    """Custom exception for error reporting."""


class YamlAssertionError(AssertionError):
    """Custom assertion error for error reporting."""


class YamlFile(pytest.File):
    def collect(self):
        raw = yaml.safe_load(self.fspath.open())
        for idx, (name, spec) in enumerate(raw.items()):
            if name in ("config", "metadata") and spec:
                yield YamlConfigItem.from_parent(self, name=name, spec=spec)
            elif name == "tests":
                for entry in spec:
                    _spec = entry.copy()
                    _spec["idx"] = idx
                    yield YamlTestItem.from_parent(self, name=entry["name"], spec=_spec)
            else:
                raise NotImplementedError


class YamlTestItem(pytest.Item):
    def __init__(self, name, parent, spec):
        super().__init__(name, parent)
        self.spec = spec

    def runtest(self):
        if not self.spec["assert"]:
            self.spec["error"] = "assert"
            raise YamlAssertionError(self, self.spec.items())

    def repr_failure(self, excinfo):
        """Called when self.runtest() raises an exception."""
        _class, item, traceback = excinfo.__dict__["_excinfo"]
        entry = dict(item.args[1])
        idx = entry.pop("idx")
        error = entry.pop("error", None)
        code = yaml.dump(entry, indent=2)
        return (
            f"\nerror = {error}\n"
            f"\n{textwrap.indent(code, prefix='    ')}"
            f"\n{self.fspath}:{idx}: {excinfo.typename}"
        )

    def reportinfo(self):
        return self.fspath, None, f"{self.name}"


class YamlConfigItem(pytest.Item):
    """A class to carry metadata configuration."""

    def __init__(self, name, parent, spec):
        super().__init__(name, parent)
        self.spec = spec
