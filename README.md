# testing-framework

A very simple prototype testing framework based on pytest.

Define test scenarios in YAML and generate reports in HTML.

## Quickstart

Install:

```sh
$ pip install .
```

To run the examples:

```sh
$ etf scenarios/example
```

Generate report in HTML:

```sh
$ etf scenarios/example/test_simple.yml --report="my_report.html"
```

### Options

You can also use the full capabilities through `pytest`:

```sh
$ pytest scenarios/example/test_simple.yml --pdb --tb=line --metadata foo bar
```

### Examples

See examples in the [docs folders](docs/README.md).
